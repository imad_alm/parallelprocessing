# -*- coding: utf-8 -*-
"""
Created on Thu Jul 29 19:32:06 2021

@author: Imad
"""

import sys
import click
import logging
from os import path, pardir
import multiprocessing as mp
import pandas as pd
from datetime import datetime
# Local imports
sys.path.append(path.abspath(path.join(path.dirname(__file__), pardir)))
sys.path.insert(1, '../common')
from dummy_ai import getCompanyAttractiveness

# Logger definition
logging.basicConfig(format="%(message)s", level=logging.INFO)

@click.command()
@click.option(
    "--filename",
    default="https://raw.githubusercontent.com/daplab-finances/yahoo-finance/master/src/main/resources/company-lists/usa/nasdaq-company-list.csv",
    help="Input CSV file name"
)
@click.option(
    "--top",
    default=10,
    help="Number of companies Symbols to print at the end"
)

@click.option(
    "--threads",
    default=60,
    help="Number of threads"
)

def main(filename: str, threads: int, top=10) -> None:
    """
    Args:
        filename (str): Input CSV file name
        top (int, optional): Number of companies Symbols to print at the end. Defaults to 10.
    """ 
    df = pd.read_csv(filename)
    df = df.iloc[:, 0]
    
    
    start = datetime.now()
   
    
    pool = mp.Pool(processes=threads) # set the number of threads
    result = pool.map(getCompanyAttractiveness, [name for name in df]) # map the function for each company names per thread
    print("The treatments tooks ", datetime.now() - start)
    result_sorted = sorted(result, key=lambda k: k['score'],reverse=True)[:top]
    
    print("the first", top, " targets are :" )
    compteur = 1
    for value in result_sorted:    
        print(compteur," id : ", value["id"], " score : ",value["score"])
        compteur+=1
            

        
        
if __name__ == "__main__":
    main()

